import datetime

import numpy as np
import pandas as pd
from haversine import haversine
from DbConnector import DbConnector
from tabulate import tabulate
import typing as ty
import read_geolife

pd.options.mode.chained_assignment = None  # default='warn'


class ExampleProgram:

    def __init__(self):
        self.connection = DbConnector()
        self.db_connection = self.connection.db_connection
        self.cursor = self.connection.cursor

    def create_table(self, table_name, query: ty.Optional = None):
        if not query:
            query = """CREATE TABLE IF NOT EXISTS %s (
                       id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
                       name VARCHAR(30))
                    """
        else:
            query = query
        # This adds table_name to the %s variable and executes the query
        self.cursor.execute(query % table_name)
        self.db_connection.commit()

    def insert_data(self, table_name):
        names = ['Bobby', 'Mc', 'McSmack', 'Board']
        for name in names:
            # Take note that the name is wrapped in '' --> '%s' because it is a string,
            # while an int would be %s etc
            query = "INSERT INTO %s (name) VALUES ('%s')"
            self.cursor.execute(query % (table_name, name))
        self.db_connection.commit()

    def insert_users(self, users):
        for user in users:
            # Take note that the name is wrapped in '' --> '%s' because it is a string,
            # while an int would be %s etc
            query = "INSERT INTO %s (id, has_labels) VALUES ('%s',%s)"
            # query = "INSERT INTO %s (name) VALUES ('%s')"
            self.cursor.execute(query % ("User", user[0], user[1]))
        self.db_connection.commit()

    def insert_labels(self, label, user_id):
        for row in label.values:
            transportation_mode = row[-1]
            start_time = row[0]
            end_time = row[1]
            query = """INSERT INTO %s (user_id, transportation_mode, start_date_time, end_date_time)
            VALUES ('%s', '%s', '%s', '%s')
            """
            self.cursor.execute(query % ("Activity", user_id, transportation_mode, start_time, end_time))
        self.db_connection.commit()

    def insert_trackpoints(self, linked_to_activity: pd.DataFrame):
        linked_to_activity.to_sql(con="mysql+mysqlconnector://testuser:test123@127.0.01:3306/STRAVA", name="TrackPoint",
                                  if_exists="append", index=False)
        # linked_to_activity.to_sql(con="server=127.0.0.1;uid=testuser;pwd=test123;database=STRAVA", name="TrackPoint", if_exists="append")
        # query = """INSERT INTO %s (activity_id, lat, lon, altitude, date_days, date_time)
        # VALUES (%s, %s, %s, %s, %s, "%s")
        # """
        # self.cursor.execute(query % ("TrackPoint", activity_id, transportation_mode, start_time, end_time))
        self.db_connection.commit()

    def fetch_data(self, query, table_name, show: bool = False):
        self.cursor.execute(query % (table_name))
        rows = self.cursor.fetchall()
        # print("Data from table %s, raw format:" % table_name)
        # print(rows)
        # Using tabulate to show the table in a nice way
        if show:
            print("Data from table %s, tabulated:" % table_name)
            print(tabulate(rows, headers=self.cursor.column_names))
        return rows

    def fetch_data_several_tables(self, query, table1, table2, show: bool = False):
        self.cursor.execute(query % (table1, table2))
        rows = self.cursor.fetchall()
        # print("Data from table %s, raw format:" % table_name)
        # print(rows)
        # Using tabulate to show the table in a nice way
        if show:
            print("Data from table %s %s, tabulated:" % (table1, table2))
            print(tabulate(rows, headers=self.cursor.column_names))
        return rows

    def drop_table(self, table_name):
        print("Dropping table %s..." % table_name)
        query = "DROP TABLE %s"
        self.cursor.execute(query % table_name)

    def show_tables(self):
        self.cursor.execute("SHOW TABLES")
        rows = self.cursor.fetchall()
        print(tabulate(rows, headers=self.cursor.column_names))

    def fetch_activities_by_user(self, user_id: str):
        # query = "SELECT * FROM %s"
        query = f"SELECT id, start_date_time, end_date_time FROM %s WHERE user_id={user_id}"
        self.cursor.execute(query % "Activity")
        rows = self.cursor.fetchall()
        print("Data from table %s, raw format:" % "Activity")
        print(rows)
        # Using tabulate to show the table in a nice way
        print("Data from table %s, tabulated:" % "Activity")
        print(tabulate(rows, headers=self.cursor.column_names))
        return rows

    def set_up_tables(self):
        user_table_name: str = "User"
        user_query = """CREATE TABLE IF NOT EXISTS %s (
                   id VARCHAR(30) NOT NULL PRIMARY KEY,
                   has_labels boolean default false)
                """

        self.create_table(table_name=user_table_name, query=user_query)
        activity_table_name: str = "Activity"
        activity_query = """
        CREATE TABLE IF NOT EXISTS %s (
                   id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
                   transportation_mode VARCHAR(30),
                   start_date_time DATETIME, 
                   end_date_time DATETIME,
                   user_id VARCHAR(30),
                   FOREIGN KEY (user_id) REFERENCES User(id)
                   )
        """
        self.create_table(table_name=activity_table_name, query=activity_query)
        trackpoint_table_name: str = "TrackPoint"
        trackpoint_query = """ CREATE TABLE IF NOT EXISTS %s (
                   id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
                   activity_id int,
                   FOREIGN KEY (activity_id) REFERENCES Activity(id), 
                   lat DOUBLE, 
                   lon DOUBLE, 
                   altitude int, 
                   date_days DOUBLE, 
                   date_time DATETIME)
        """
        self.create_table(table_name=trackpoint_table_name, query=trackpoint_query)


def create_users() -> ty.List[ty.Tuple[str, bool]]:
    users: ty.List[ty.Tuple[str, bool]] = []
    labeled_users = pd.read_csv("../dataset/labeled_ids.txt", delimiter="\t", header=None)
    for i in range(182):
        if i < 100:
            id: str = "0" + str(i)
            if i < 10:
                id = "00" + str(i)
        else:
            id = str(i)
        if i in labeled_users.values:
            if i > 100:
                users.append((id, True))
            else:
                users.append((id, True))
        else:
            users.append((id, False))
    return users


def get_user_activities(user: str, main_folder: str, has_labels: bool):
    if has_labels:
        path: str = main_folder + user + "/labels.txt"
        user_labels: pd.DataFrame = read_geolife.read_labels(path)
    else:
        path: str = main_folder + user
        user_labels: pd.DataFrame = read_geolife.read_user(path, has_labels)

    return user_labels


class Setup:
    def __init__(self):
        self.connection = DbConnector()
        self.db_connection = self.connection.db_connection
        self.cursor = self.connection.cursor
        self.main_data_folder: str = "./dataset/Data/"
        self.program = ExampleProgram()

    def get_user_trackpoints(self, user_id):
        path: str = self.main_data_folder + user_id
        trackpoints: pd.DataFrame = read_geolife.read_user(path)
        return trackpoints

    def setup_and_insert_data(self):
        # sets up the tables
        self.program.set_up_tables()

        # retreives the users and inserts to database
        users = create_users()
        self.program.insert_users(users)

        # retreives activities and inserts to database
        self.setup_and_insert_activites(users)

        # retreives Trackpoints and inserts to database
        self.setup_and_insert_trackpoints(users)

    def setup_and_insert_activites(self, users: ty.List):
        for user in users:
            print("At user", user[0])
            user_id = user[0]
            if user[1]:
                user_labels = get_user_activities(user_id, self.main_data_folder, user[1])
                self.program.insert_labels(user_labels, user_id)
            else:
                user_data = get_user_activities(user_id, self.main_data_folder, user[1])
                self.program.insert_labels(user_data, user_id)

    def setup_and_insert_trackpoints(self, users: ty.List):
        for user in users[49:]:
            print("At user", user[0])
            user_id = user[0]
            user_trackpoints = self.get_user_trackpoints(user_id=user_id)
            if len(user_trackpoints) == 0: continue
            user_trackpoints.sort_values(by="time", inplace=True)
            user_activities = self.program.fetch_activities_by_user(user_id=user_id)
            for row in user_activities:
                activity_id, start_time, end_time = row[0], row[1], row[2]
                linked_to_activity = user_trackpoints.query(f'"{start_time}" <= time <= "{end_time}"')
                if not linked_to_activity.empty:
                    linked_to_activity.rename(columns={"time": "date_time", "alt": "altitude"}, inplace=True)
                    linked_to_activity["date_days"] = 0
                    linked_to_activity["activity_id"] = activity_id
                    linked_to_activity.drop(["label"], axis=1, inplace=True)
                    self.convert_to_day(linked_to_activity)
                    self.program.insert_trackpoints(linked_to_activity)

    def convert_to_day(self, linked_to_activity: pd.DataFrame):
        result = [self.calculate_to_day(time) for time in linked_to_activity["date_time"]]
        linked_to_activity["date_days"] = result

    def calculate_to_day(self, trackpoint_time):
        nanosec_in_day: float = 86400000000000
        start_time = datetime.datetime(1899, 12, 30)
        days_since = trackpoint_time - start_time
        return days_since.value / nanosec_in_day


def drop_tables():
    program = ExampleProgram()
    table_name_to_drop: ty.List = ["User"]
    for table in table_name_to_drop:
        program.drop_table(table)


def Setup_database():
    try:
        setup = Setup()
        setup.setup_and_insert_data()
    except Exception as e:
        print("ERROR: Failed to use database:", e)
    # finally:
    #     if setup.program:
    #         setup.program.connection.close_connection()


def queries():
    setup = Setup()

    # QUESTION 1
    question_1(setup)

    # QUESTION 2
    question_2(setup)

    # QUESTION 3
    question_3(setup)

    # QUESTION 4
    question_4(setup)

    # QUESTION 5
    question_5(setup)

    # QUESTION 6
    # A and B
    question_6(setup)

    # QUESTION 7
    question_7()

    # Question 8
    question_8()

    # Question 9
    question_9()

    # Question 10
    question_10()

    # Question 11
    question_11()

def question_11():
    query = "SELECT Activity.user_id, Activity.transportation_mode, COUNT(Activity.transportation_mode) as TransportationCounter FROM Activity " \
            "WHERE Activity.transportation_mode != '0' GROUP BY Activity.transportation_mode, Activity.user_id"
    q_11_df: pd.DataFrame = pd.read_sql(sql=query, con="mysql+mysqlconnector://testuser:test123@127.0.01:3306/STRAVA")
    df = q_11_df.loc[q_11_df.groupby("user_id")["TransportationCounter"].idxmax()]
    print(df.to_string())
def question_10():
    query = "SELECT Activity.user_id, TrackPoint.lon, TrackPoint.lat FROM Activity INNER JOIN TrackPoint " \
            "ON Activity.id=TrackPoint.activity_id HAVING TrackPoint.lon BETWEEN 116.397 and 116.398 AND TrackPoint.lat BETWEEN 39.916 and 39.917"
    q10_df: pd.DataFrame = pd.read_sql(sql=query,
                                       con="mysql+mysqlconnector://testuser:test123@127.0.01:3306/STRAVA")

    print("Users in the forbidden city of Bejing: ", q10_df["user_id"].unique())


def question_9():
    query = "SELECT Activity.user_id, TrackPoint.activity_id, TrackPoint.date_time FROM Activity INNER JOIN TrackPoint " \
            "ON Activity.id=TrackPoint.activity_id"
    q11_df: pd.DataFrame = pd.read_sql(sql=query, con="mysql+mysqlconnector://testuser:test123@127.0.01:3306/STRAVA")
    invalid_user = {}
    first_user = ""
    tmp_df = pd.DataFrame(columns=["user_id", "counter"])
    activities_invalid = []
    for i in range(0, len(q11_df) - 1):
        fmt = '%Y-%m-%d %H:%M:%S'
        activity_id = q11_df.loc[i, "activity_id"]
        if (q11_df.loc[i, "activity_id"] == q11_df.loc[i + 1, "activity_id"]):
            user_id = q11_df.loc[i, "user_id"]
            if first_user != user_id:
                print("At user: ", user_id)
                first_user = user_id
            date1: datetime.datetime = datetime.datetime.strptime(str(q11_df.loc[i, "date_time"]), fmt)
            date2: datetime.datetime = datetime.datetime.strptime(str(q11_df.loc[i + 1, "date_time"]), fmt)
            diff = date2 - date1
            td_mins = int(round(diff.total_seconds() / 60))
            if (td_mins) > 5:
                if not user_id in tmp_df["user_id"].values:
                    tmp_df.loc[user_id] = [str(user_id), 1]
                    activities_invalid.append(activity_id)
                if user_id in tmp_df["user_id"].values and activity_id not in activities_invalid:
                    counter = tmp_df.loc[tmp_df["user_id"] == user_id]["counter"]
                    location = tmp_df.index[tmp_df["user_id"] == user_id].tolist()
                    test = counter[-1] + 1
                    tmp_df.at[location[0], "counter"] = test
                    activities_invalid.append(activity_id)


    # q11_df["invalid"] = [date_time for date_time in q11_df.groupby("user_id")["date_time"]]
    print(tmp_df.to_string())
    print(invalid_user)
    df = pd.DataFrame.from_dict(invalid_user)
    print(df.to_string())


def question_8():
    table1 = "Activity"
    table2 = "TrackPoint"
    query = "SELECT Activity.user_id, TrackPoint.activity_id, MAX(TrackPoint.altitude) - MIN(TrackPoint.altitude) as altitude FROM Activity INNER JOIN TrackPoint ON Activity.id=TrackPoint.activity_id WHERE NOT TrackPoint.Altitude = -777 GROUP BY TrackPoint.activity_id"
    # query = "SELECT Activity.user_id, TrackPoint.activity_id, TrackPoint.altitude as altitude FROM Activity INNER JOIN TrackPoint ON Activity.id=TrackPoint.activity_id WHERE NOT TrackPoint.altitude = -777 GROUP BY activity_id"
    df: pd.DataFrame = pd.read_sql(sql=query,
                                   con="mysql+mysqlconnector://testuser:test123@127.0.01:3306/STRAVA")
    # Creates a new column to add the calculated difference between the lat and lon for each row.
    df = df.groupby("user_id", as_index=False)["altitude"].sum()
    df["altitude"] = [(altitude / 3.281) for altitude in df["altitude"]]
    df.sort_values("altitude", inplace=True, ascending=False)
    print(df.head(20))


def question_7():
    table1 = "Activity"
    table2 = "TrackPoint"
    query = "SELECT EXTRACT(YEAR FROM Activity.start_date_time) AS year, Activity.Transportation_mode, Activity.user_id, TrackPoint.activity_id, TrackPoint.lat, TrackPoint.lon" \
            " FROM Activity INNER JOIN TrackPoint ON Activity.id=TrackPoint.activity_id HAVING Activity.user_id=112 AND year=2008 AND Activity.transportation_mode='walk'"
    user_112_df: pd.DataFrame = pd.read_sql(sql=query,
                                            con="mysql+mysqlconnector://testuser:test123@127.0.01:3306/STRAVA")
    # Creates a new column to add the calculated difference between the lat and lon for each row.
    user_112_df["distance"] = 0
    for i in range(0, len(user_112_df) - 1):
        if (user_112_df.loc[i, "activity_id"] == user_112_df.loc[i + 1, "activity_id"]):
            position1 = (user_112_df.loc[i, "lat"], user_112_df.loc[i, "lon"])
            position2 = (user_112_df.loc[i + 1, "lat"], user_112_df.loc[i + 1, "lon"])
            user_112_df.loc[i, "distance"] = haversine(position1, position2)
    print(user_112_df.head(10))
    sum = user_112_df["distance"].sum()
    print("Total distance walked by user 112 is: ", sum)


def question_6(setup):
    query = "SELECT EXTRACT(year FROM start_date_time) AS year, COUNT(*) FROM %s GROUP BY year ORDER BY COUNT(*) DESC"
    table: str = "Activity"
    test = setup.program.fetch_data(query, table_name=table, show=True)
    # B
    query = "SELECT EXTRACT(year from date_time) as year, COUNT(*) FROM %s GROUP BY year ORDER BY COUNT(*) DESC"
    table: str = "TrackPoint"
    test = setup.program.fetch_data(query, table_name=table, show=True)


def question_5(setup):
    query = "SELECT transportation_mode, COUNT(*) FROM %s GROUP BY transportation_mode ORDER BY COUNT(*) DESC"
    table: str = "Activity"
    test = setup.program.fetch_data(query, table_name=table, show=True)


def question_4(setup):
    query = "SELECT user_id FROM %s WHERE transportation_mode='taxi' GROUP BY user_id"
    table: str = "Activity"
    test = setup.program.fetch_data(query, table_name=table, show=True)


def question_3(setup):
    query = "SELECT user_id FROM %s GROUP BY user_id ORDER BY COUNT(*) DESC LIMIT 20"
    tables = ["Activity"]
    for table in tables:
        test = setup.program.fetch_data(query, table_name=table, show=True)


def question_2(setup):
    tables = ["User", "Activity"]
    query = "SELECT COUNT(*) FROM %s"
    tmp_list = []
    for table in tables:
        test = setup.program.fetch_data(query, table_name=table, show=True)
        tmp_list.append(test[0][0])
    average = tmp_list[1] / tmp_list[0]
    print("Average number of activities per user: ", average)


def question_1(setup):
    query = "SELECT COUNT(*) FROM %s"
    tables = ["User", "Activity", "TrackPoint"]
    for table in tables:
        setup.program.fetch_data(query, table_name=table, show=True)


if __name__ == '__main__':
    Setup_database()
    queries()
