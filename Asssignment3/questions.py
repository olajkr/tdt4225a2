import datetime
import typing as ty

import haversine as haversine
import pandas as pd
import pymongo.collection

from Asssignment3.A3Databasehandling import Database
from Asssignment3.A3Datacleaning import Setup
from Asssignment3.A3DbConnector import DbConnector
from pprint import pprint
from haversine import haversine


class Questions():
    def __init__(self):
        self.connection = DbConnector()
        self.mongodb = Database()
        self.user_col = self.connection.db["User"]
        self.act_col = self.connection.db["Activities"]
        self.trp_col = self.connection.db["TrackPoint"]

    def question_1(self):
        """How many users, activites, and trackpoints are there in the dataset (after inserted intp the database)"""
        print("Number of User documents: ", self.user_col.count_documents({}))
        print("Number of Activity documents: ", self.act_col.count_documents({}))
        print("Number of TrackPoint documents: ", self.trp_col.count_documents({}))

    def question_2(self):
        """Find the avarage number of activities per user"""
        print("Average activities per user: ",
              self.act_col.count_documents({}) / self.user_col.count_documents({}))

    def question_3(self):
        """Find the top 20 users with the highest number of activities"""
        # count = self.user_col.find().sort("activities", pymongo.DESCENDING)
        pipeline = [
            {"$project": {"_id": "$_id", "activities size": {"$size": "$activities"}}},
            {"$sort": {"activities size": -1}}
        ]

        users_act_count = self.user_col.aggregate(pipeline)
        df = pd.DataFrame(list(users_act_count))
        print(df.to_string())

    def question_4(self):
        """Find all users who have taken a taxi"""
        pipeline = [
            {"$match": {"label": "taxi"}},
            {"$lookup": {
                "from": "User",
                "localField": "_id",
                "foreignField": "owner_id",
                "as": "User_activities"
            }},
            {"$group": {
                "_id": "$owner_id"
            }},
            {"$sort": {"_id": 1}}
        ]
        activities = self.act_col.aggregate(pipeline)
        df = pd.DataFrame(list(activities))
        print(df.to_string())

    def question_5(self):
        """Find all types of transportation modes and count how many activities that are tagged
        with these transportation mode labels. Do not count the rows where the mode is null"""
        pipeline = [
            {"$match": {
                "label": {
                    "$not": {"$eq": "0"}
                }
            }},
            {"$group": {
                "_id": "$label",
                "count": {"$sum": 1}
            }},
            {"$sort": {
                "count": -1}
            }
        ]
        activities = self.act_col.aggregate(pipeline)
        df = pd.DataFrame(list(activities))
        print(df.to_string())

    def question_6(self):
        """
        A)Find the year with the most activities
        B) Is this also the year with the most recorded hours?
        """
        # A
        pipeline = [
            {"$project": {
                "extracted_year": {"$substr": ["$start_date", 0, 4]}
            }},
            {"$group": {
                "_id": "$extracted_year",
                "count": {"$sum": 1}
            }}, {"$sort": {
                "count": -1
            }}
        ]
        activities = self.act_col.aggregate(pipeline)
        df = pd.DataFrame(list(activities))
        print("Results A: ", df.to_string())

        # B
        pipeline = [
            {"$project": {
                "extracted_year": {"$substr": ["$date_time", 0, 4]}
            }},
            {"$group": {
                "_id": "$extracted_year",
                "count": {"$sum": 1}
            }}, {"$sort": {
                "count": -1
            }}
        ]
        trackpoints = self.trp_col.aggregate(pipeline)
        df = pd.DataFrame(list(trackpoints))
        print("Results B: ", df.to_string())

    def question_7(self):
        """Find the total distance (in km) walked in 2008, by user with id=112"""
        user_id = 112
        user_id_str = f"{user_id:03}"

        activities_id_walk = set(
            doc['_id'] for doc in
            self.act_col.find(
                {
                    '$and': [
                        # "$substr": ["date_time", 0, 4] // Kanskje dette funker
                        {'label': 'walk'},
                        {'owner_id': user_id_str},
                    ]
                }
            )
        )
        trackpoints_all = list(
            self.trp_col.find(
                {
                    '$and': [
                        {'owner_id': user_id_str},
                        {'date_time': {'$gte': datetime.datetime(2008, 1, 1)}},
                        {'date_time': {'$lt': datetime.datetime(2009, 1, 1)}},
                    ]
                }
            )
        )

        tmp = []
        for tp in trackpoints_all:
            if tp['activity_id'] in activities_id_walk:
                tmp.append(tp)
        trackpoints = tmp

        df = pd.DataFrame(list(trackpoints))
        print(df.head())
        df["distance"] = 0
        for i in range(0, len(df) - 1):
            if (df.loc[i, "activity_id"] == df.loc[i + 1, "activity_id"]):
                position1 = (df.loc[i, "lat"], df.loc[i, "lon"])
                position2 = (df.loc[i + 1, "lat"], df.loc[i + 1, "lon"])
                df.loc[i, "distance"] = haversine(position1, position2)
        sum = df["distance"].sum()
        print("Total distance walked by user 112 is: ", sum)

        # trackpoints = list(
        #     trackpoint for trackpoint in trackpoints_all
        #     if trackpoint['activity_id'] in activities_id_walk
        # )
        # pipeline = [
        #     {"$match": {
        #         "user_id": "112",
        #     }},
        #     {"$lookup": {
        #         "from": "Activities",
        #         "localField": "activity_id",
        #         "foreignField": "_id",
        #         "as": "Trackpoint_activities"
        #     }},
        #     {"$project": {
        #         "extracted_year": {
        #             "$substr": ["$date_time", 0, 4]
        #         },
        #         "lat": "$lat",
        #         "lon": "$lon",
        #         "activity_id": "$activity_id",
        #         # "label": "$label"
        #     }},
        #     {"$match": {
        #         "extracted_year": "2008",
        #         # "label": "label"
        #     }},
        #
        # ]
        # pd.set_option('display.max_columns', None)
        # trackpoints = list(self.trp_col.aggregate(pipeline))



    def question_9(self):
        """Find the users with invalid activities (more than 5 minutes between two consecutive trackpoints)"""

        # def is_invalid(trackpoints: ty.List[dict]):
        #     trackpoints = sorted(trackpoints, key=lambda trackpoint: trackpoint['date_time'])
        #     for cur_trackpoint, next_trackpoint in zip(trackpoints, trackpoints[1:]):
        #         if next_trackpoint['date_time'] - cur_trackpoint['date_time'] > datetime.timedelta(minutes=5):
        #             return True
        #     return False

        df: pd.DataFrame = pd.DataFrame(list(self.trp_col.find(limit=500000)))
        df.groupby("activity_id")
        activities: ty.Dict[str, ] = df.loc[:, ["activity_id", "date_time", "user_id"]]
        # activities: ty.Dict[ObjectID, ty.List[Trackpoints]] = {}
        invalid = []
        fmt = '%Y-%m-%d %H:%M:%S'
        for i in range(0, len(df) - 1):
                row1 = df.loc[i]
                row2 = df.loc[i + 1]
                if row1["activity_id"] == row2["activity_id"]:
                    date1: datetime.datetime = datetime.datetime.strptime(str(row1["date_time"]), fmt)
                    date2: datetime.datetime = datetime.datetime.strptime(str(row2 ["date_time"]), fmt)
                    diff = date2 - date1
                    td_mins = int(round(diff.total_seconds() / 60))
                    if td_mins > 5:
                        if row1['date_time'] - row2['date_time'] > datetime.timedelta(minutes=5):
                            if row1["user_id"] not in invalid:
                                invalid.append(row1["user_id"])

        print("Invalid Users: ", invalid)

        # for activity, trackpoints in activities.items():
        #     if is_invalid(trackpoints):
        #         invalid.append(activity)
        # return invalid



#{'$and': [{'label': 'walk'}, {'owner_id': '112'}]}

if __name__ == '__main__':
    # setup = Setup()
    # setup.setup_and_insert_data()
    q = Questions()
    # q.question_1()
    # q.question_2()
    # q.question_3()
    # q.question_4()
    # q.question_5()
    # q.question_6()
    # q.question_7()
    q.question_9()
