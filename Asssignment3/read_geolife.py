import typing as ty
import numpy as np
import pandas as pd
import glob
import os.path
import datetime
import os


def read_plt(plt_file, has_labels):
    points = pd.read_csv(plt_file, skiprows=6, header=None,
                         parse_dates=[[5, 6]], infer_datetime_format=True)

    # for clarity rename columns
    points.rename(inplace=True, columns={'5_6': 'time', 0: 'lat', 1: 'lon', 3: 'alt'})

    # remove unused columns
    points.drop(inplace=True, columns=[2, 4])
    if not has_labels:
        points["end_time"] = points["time"].iloc[-1]
        points.rename(columns={"time": "start_time"}, inplace=True)
        points.drop(columns=["lat", "lon", "alt"], inplace=True)
        return points.head(1)

    return points



mode_names = ['walk', 'bike', 'bus', 'car', 'subway', 'train', 'airplane', 'boat', 'run', 'motorcycle', 'taxi']
mode_ids = {s: i + 1 for i, s in enumerate(mode_names)}


def read_labels(labels_file):
    labels = pd.read_csv(labels_file, skiprows=1, header=None,
                         parse_dates=[[0, 1], [2, 3]],
                         infer_datetime_format=True, delim_whitespace=True)

    # for clarity rename columns
    labels.columns = ['start_time', 'end_time', 'label']

    # replace 'label' column with integer encoding
    # labels['label'] = [mode_ids[i] for i in labels['label']]

    return labels


def apply_labels(points, labels, has_labels: bool = False):
    indices = labels['start_time'].searchsorted(points['time'], side='right') - 1
    no_label = (indices < 0) | (points['time'].values >= labels['end_time'].iloc[indices].values)
    points['label'] = labels['label'].iloc[indices].values
    points['label'][no_label] = 0


def read_user(user_folder, has_labels: bool = True) -> ty.Union[list, pd.DataFrame]:
    labels = None

    plt_files = glob.glob(os.path.join(user_folder, 'Trajectory', '*.plt'))
    plt_to_csv = []
    for f in plt_files:
        #makes sure that we only add data where the length is less than 2500 trackpoints.
        cleaned_plt = read_plt(f, has_labels)
        if len(cleaned_plt) < 6 + 2500:
            plt_to_csv.append(cleaned_plt)
    if not plt_to_csv:
        return plt_to_csv
    else:
        df = pd.concat(plt_to_csv)
    labels_file = os.path.join(user_folder, 'labels.txt')
    if os.path.exists(labels_file):
        labels = read_labels(labels_file)
        apply_labels(df, labels)
    else:
        df['label'] = 0

    return df


def read_all_users(folder):
    subfolders = os.listdir(folder)
    dfs = []
    for i, sf in enumerate(subfolders):
        print('[%d/%d] processing user %s' % (i + 1, len(subfolders), sf))
        df = read_user(os.path.join(folder, sf))
        df['user'] = int(sf)
        dfs.append(df)
    return pd.concat(dfs)
