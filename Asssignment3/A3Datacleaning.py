import datetime
import random
import typing as ty
from uuid import uuid4

import bson
import pandas as pd

from Asssignment3 import read_geolife
from Asssignment3.A3Databasehandling import Database
from Asssignment3.A3DbConnector import DbConnector

pd.options.mode.chained_assignment = None  # default='warn'


class Setup:
    def __init__(self):
        self.connection = DbConnector()
        #   self.db_connection = self.connection.db_connection
        # self.cursor = self.connection.cursor
        self.main_data_folder: str = "../dataset/Data/"
        self.labeled_ids_file: str = "../dataset/labeled_ids.txt"
        self.mongodb = Database()
        self.random = random.seed(10)

    def get_user_trackpoints(self, user_id) -> ty.Optional[pd.DataFrame]:
        path: str = self.main_data_folder + user_id
        trackpoints: pd.DataFrame = read_geolife.read_user(path)
        if not isinstance(trackpoints, pd.DataFrame):
            return None
        return trackpoints

    def convert_to_day(self, linked_to_activity: pd.DataFrame):
        result = [self.calculate_to_day(time) for time in linked_to_activity["date_time"]]
        linked_to_activity["date_days"] = result

    def calculate_to_day(self, trackpoint_time):
        nanosec_in_day: float = 86400000000000
        start_time = datetime.datetime(1899, 12, 30)
        days_since = trackpoint_time - start_time
        return days_since.value / nanosec_in_day

    def setup_and_insert_data(self):
        users = self.create_user_list()
        for (user_id, has_labels) in users:
            print("Inserts data for user: ", user_id)
            activities: pd.DataFrame = self.get_user_activities(user=user_id, has_labels=has_labels)
            trackpoints: ty.Optional[pd.DataFrame] = self.get_user_trackpoints(user_id=user_id)
            activities = activities.applymap(str)
            activities, object_ids_activities = self.generate_random_objectid(activities)
            activities["owner_id"] = user_id
            # activity_dict = activities.to_json(orient="records")
            user = {
                "_id": user_id,
                "has_labels": has_labels,
                "activities": object_ids_activities
            }

            self.mongodb.insert_data_one(user, "User")
            self.mongodb.insert_data_many(activities.to_dict("records"), "Activities")
            if trackpoints is not None:
                trackpoints["owner_id"] = user_id
                self.trackpoint_preprocessing(activities, trackpoints, user_id)

    def trackpoint_preprocessing(self, activities: pd.DataFrame, trackpoints: pd.DataFrame, user_id):
        for (idx, row) in activities.iterrows():
            # makes sure we create a unique id
            start_time, end_time, activity_id = row.loc["start_time"], row.loc["end_time"], row.loc["_id"]

            try:
                linked_to_activity = trackpoints.query(f'"{start_time}" <= time <= "{end_time}"')
                if not linked_to_activity.empty:
                    linked_to_activity.rename(columns={"time": "date_time", "alt": "altitude"}, inplace=True)
                    linked_to_activity["date_days"] = 0
                    linked_to_activity["activity_id"] = activity_id
                    linked_to_activity.drop(["label"], axis=1, inplace=True)
                    linked_to_activity["user_id"] = user_id
                    self.convert_to_day(linked_to_activity)
                    self.mongodb.insert_data_many(linked_to_activity.to_dict("records"), "TrackPoint")
            except Exception as e:
                print(f"Got exception: {e}")
                continue

    def get_user_activities(self, user: str, has_labels: bool):
        if has_labels:
            path: str = self.main_data_folder + user + "/labels.txt"
            user_labels: pd.DataFrame = read_geolife.read_labels(path)
        else:
            path: str = self.main_data_folder + user
            user_labels: pd.DataFrame = read_geolife.read_user(path, has_labels)

        return user_labels

    def generate_random_objectid(self, df: pd.DataFrame) -> (pd.DataFrame, ty.List):
        id_list = [bson.ObjectId() for _ in range(len(df))]
        df["_id"] = id_list

        return df, id_list

    def create_user_list(self) -> ty.List[ty.Tuple[str, int]]:
        labeled_users = pd.read_csv(self.labeled_ids_file, delimiter="\t", header=None)
        users: ty.List[ty.Tuple[str, bool]] = []
        for i in range(182):
            users.append((f"{i:03}", i in labeled_users.values))
        return users

# except Exception as e:
#     print("ERROR: Failed to use database:", e)
# finally:
#     if setup.program:
#         setup.program.connection.close_connection()
