from pprint import pprint

import pandas as pd
import typing as ty
from A3DbConnector import DbConnector


class Database:

    def __init__(self):
        self.connection = DbConnector()
        self.client = self.connection.client
        self.db = self.connection.db

    def create_coll(self, collection_name):
        collection = self.db.create_collection(collection_name)
        print('Created collection: ', collection)

    def insert_trackpoints(self, linked_to_activity):
        pass

    def insert_data_many(self, data: ty.List[ty.Dict], collection_name):
        self.add_collection(collection_name)
        collection = self.db[collection_name]
        collection.insert_many(data)

    def add_collection(self, collection_name):
        list_of_collections = self.db.list_collection_names()  # Return a list of collections in 'test_db'
        if collection_name not in list_of_collections:
            self.create_coll(collection_name=collection_name)

    def insert_data_one(self, data: ty.Dict, collection_name):
        self.add_collection(collection_name)
        collection = self.db[collection_name]
        collection.insert_one(data)

    def fetch(self, collection_name: str, query):
        results = self.db[collection_name].count_documents()
        return results

    def fetch_activities_by_user(self, collection_name: str, query):
        collection = self.db[collection_name]
        documents = collection.find(query)
        df = pd.DataFrame(list(documents))
        return df

    def insert_documents(self, collection_name):
        docs = [
            {
                "_id": 1,
                "name": "Bobby",
                "courses":
                    [
                        {'code': 'TDT4225', 'name': ' Very Large, Distributed Data Volumes'},
                        {'code': 'BOI1001', 'name': ' How to become a boi or boierinnaa'}
                    ]
            },
            {
                "_id": 2,
                "name": "Bobby",
                "courses":
                    [
                        {'code': 'TDT02', 'name': ' Advanced, Distributed Systems'},
                    ]
            },
            {
                "_id": 3,
                "name": "Bobby",
            }
        ]
        collection = self.db[collection_name]
        collection.insert_many(docs)

    def fetch_documents(self, collection_name):
        collection = self.db[collection_name]
        documents = collection.find({})
        for doc in documents:
            pprint(doc)

    def drop_coll(self, collection_name):
        collection = self.db[collection_name]
        collection.drop()

    def show_coll(self):
        collections = self.client['test'].list_collection_names()
        print(collections)
